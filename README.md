# Mini Project 8: Rust Command-Line Tool with Testing

## Goals
- Rust command-line tool
- Data ingestion/processing
- Unit tests

## Project Description
This project presents a Rust command-line tool for reading and processing data from a CSV file containing information about historical figures. It includes functionalities for reading figure data from the CSV, searching for figures by name, and outputting figure information.

## Project Setup
1. Create a new Rust project with `--bin` flag to produce the binary executable for the command line tool and navigate into it.
```bash
cargo new <Your Project Name> --bin && cd <Your Project Name>
```
2. Create the data file used for the program. My project used a historical_figures.csv file containing historical figures and their information:
```csv
Name,Nationality,Birth Year,Death Year,Notable Work,Field
Leonardo da Vinci,Italian,1452,1519,Mona Lisa,Art/Science
...
```
3. Add the following dependency to the `Cargo.toml` file to handle CSV data.
```toml
[dependencies]
csv = "1.1.6"
```
4. Implement the program functionality in `main.rs`, focusing on processing and querying the historical figures data.

## Tool Functionality
1. **Build the Program:**
    Compile the program with:
    ```bash
    cargo build
    ```
   
2. **Search for a Historical Figure:**
    - To run our program and search for a historical figure by name, we can directly execute the binary:
      ```bash
      ./target/debug/mini_project8 "<figure name>"
      ```
      For example:
      ```bash
      ./target/debug/mini_project8 "Leonardo da Vinci"
      ```
    - Alternatively, use `cargo run` for compiling and running in one step, ensuring you're executing the most current version of your code:
      ```bash
      cargo run -- "<figure name>"
      ```
      For instance:
      ```bash
      cargo run -- "Leonardo da Vinci"
      ```

    If the figure is not found, the program will output that the historical figure was not found.


## Data Ingestion

Data ingestion is achieved through the `read_historical_figures_from_csv` function, which reads data from a specified CSV file:

- **File Reading:** A `File` object is created by opening the specified CSV file. This step initiates the data ingestion process.
- **CSV Parsing:** The `csv::ReaderBuilder` constructs a CSV reader from the `File` object. This reader is configured to iterate over each record in the CSV, effectively parsing the CSV data.
- **Record Handling:** For each record, error handling is performed to ensure robust ingestion. Each valid record is then passed on for processing.

## Data Processing

Data processing transforms raw CSV records into structured `HistoricalFigure` instances:

- **Struct Creation:** The `HistoricalFigure::new` function takes a `csv::StringRecord` and extracts each field, mapping them to the corresponding fields of the `HistoricalFigure` struct. This structured approach allows for more complex processing and querying of the data.
- **Vector Storage:** Each `HistoricalFigure` instance is pushed into a vector, creating a collection of historical figures. This collection can then be searched and manipulated using Rust's powerful collection and iteration capabilities.

## Unit Tests

To ensure the reliability and correctness of our command-line tool, we've implemented unit tests focusing on key functionalities, particularly the ability to search for historical figures by name. These tests verify both the presence and absence of figures in our dataset, as well as the case-insensitivity of our search function.

### Detailed Steps
1. Testing Existing Figures:

  In the function `test_find_historical_figure_by_name_existing()`, we test if the tool correctly finds a figure by name when it exists in the dataset. This ensures our search functionality is accurate for known entries.

2. Testing Case-Insensitive Searches:

  It's important that our search functionality is user-friendly; thus, using the function `test_find_historical_figure_by_name_case_insensitive()`, we test that it is case-insensitive, ensuring users can find figures regardless of how they capitalize the input.

3. Testing for Non-Existent Figures:

  We use the function `test_find_historical_figure_not_found()` to ensure our tool gracefully handles searches for figures not present in our dataset is crucial for providing accurate feedback to users.

### Running Tests

To execute all unit tests and verify the integrity and functionality of our tool, use the following command:
```bash
make test
```

## Deliverables

- Sample output:
  ![found](screenshots/foundResults.png)

  ![not found](screenshots/notExist.png)

- Testing report
  ![test report](screenshots/testReport.png)