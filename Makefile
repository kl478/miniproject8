format:
	cargo fmt --quiet

lint:
	cargo clippy --quiet

test:
	cargo test

run:
	cargo run -- "Marie Curie"

all: format lint test run
