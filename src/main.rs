use csv::ReaderBuilder;
use std::error::Error;
use std::fs::File;

// Define a struct to represent a historical figure
#[derive(Debug, PartialEq)]
struct HistoricalFigure {
    name: String,
    nationality: String,
    birth_year: String,
    death_year: String,
    notable_work: String,
    field: String,
}

impl HistoricalFigure {
    // Function to create a new historical figure from CSV record
    fn new(record: csv::StringRecord) -> HistoricalFigure {
        HistoricalFigure {
            name: record.get(0).unwrap_or("").to_string(),
            nationality: record.get(1).unwrap_or("").to_string(),
            birth_year: record.get(2).unwrap_or("").to_string(),
            death_year: record.get(3).unwrap_or("").to_string(),
            notable_work: record.get(4).unwrap_or("").to_string(),
            field: record.get(5).unwrap_or("").to_string(),
        }
    }
}

// Function to read historical figures from CSV and return vector of figures
fn read_historical_figures_from_csv(
    file_path: &str,
) -> Result<Vec<HistoricalFigure>, Box<dyn Error>> {
    let mut figures = Vec::new();
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);
    for result in rdr.records() {
        let record = result?;
        let figure = HistoricalFigure::new(record);
        figures.push(figure);
    }
    Ok(figures)
}

// Function to find a historical figure by name (case-insensitive)
fn find_historical_figure_by_name<'a>(
    figures: &'a [HistoricalFigure],
    name: &str,
) -> Option<&'a HistoricalFigure> {
    figures
        .iter()
        .find(|f| f.name.to_lowercase() == name.to_lowercase())
}

fn main() {
    // Read historical figures from CSV
    let figures = match read_historical_figures_from_csv("./historical_figures.csv") {
        Ok(figs) => figs,
        Err(err) => {
            eprintln!("Error reading historical figures: {}", err);
            return;
        }
    };

    // Get figure name from command line argument
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <figure_name>", args[0]);
        return;
    }
    let figure_name = &args[1];

    // Find figure by name and print information
    match find_historical_figure_by_name(&figures, figure_name) {
        Some(figure) => println!("{:?}", figure),
        None => println!("Historical figure not found"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_historical_figure_by_name_existing() {
        let figures = vec![
            HistoricalFigure {
                name: "Leonardo da Vinci".to_string(),
                nationality: "Italian".to_string(),
                birth_year: "1452".to_string(),
                death_year: "1519".to_string(),
                notable_work: "Mona Lisa".to_string(),
                field: "Art/Science".to_string(),
            },
            HistoricalFigure {
                name: "Marie Curie".to_string(),
                nationality: "Polish".to_string(),
                birth_year: "1867".to_string(),
                death_year: "1934".to_string(),
                notable_work: "Radium".to_string(),
                field: "Science".to_string(),
            },
        ];

        assert_eq!(
            find_historical_figure_by_name(&figures, "Marie Curie"),
            Some(&HistoricalFigure {
                name: "Marie Curie".to_string(),
                nationality: "Polish".to_string(),
                birth_year: "1867".to_string(),
                death_year: "1934".to_string(),
                notable_work: "Radium".to_string(),
                field: "Science".to_string(),
            })
        );
    }

    #[test]
    fn test_find_historical_figure_by_name_case_insensitive() {
        let figures = vec![
            HistoricalFigure {
                name: "Leonardo da Vinci".to_string(),
                nationality: "Italian".to_string(),
                birth_year: "1452".to_string(),
                death_year: "1519".to_string(),
                notable_work: "Mona Lisa".to_string(),
                field: "Art/Science".to_string(),
            },
            HistoricalFigure {
                name: "Marie Curie".to_string(),
                nationality: "Polish".to_string(),
                birth_year: "1867".to_string(),
                death_year: "1934".to_string(),
                notable_work: "Radium".to_string(),
                field: "Science".to_string(),
            },
        ];

        assert_eq!(
            find_historical_figure_by_name(&figures, "marie curie"),
            Some(&HistoricalFigure {
                name: "Marie Curie".to_string(),
                nationality: "Polish".to_string(),
                birth_year: "1867".to_string(),
                death_year: "1934".to_string(),
                notable_work: "Radium".to_string(),
                field: "Science".to_string(),
            })
        );
    }

    #[test]
    fn test_find_historical_figure_not_found() {
        let figures = vec![
            HistoricalFigure {
                name: "Leonardo da Vinci".to_string(),
                nationality: "Italian".to_string(),
                birth_year: "1452".to_string(),
                death_year: "1519".to_string(),
                notable_work: "Mona Lisa".to_string(),
                field: "Art/Science".to_string(),
            },
            HistoricalFigure {
                name: "Marie Curie".to_string(),
                nationality: "Polish".to_string(),
                birth_year: "1867".to_string(),
                death_year: "1934".to_string(),
                notable_work: "Radium".to_string(),
                field: "Science".to_string(),
            },
        ];

        assert_eq!(
            find_historical_figure_by_name(&figures, "Nikola Tesla"),
            None
        );
    }
}
